package ro.orangeStartIT;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // Create a single class with a main method
        ArrayList MyHotelEmployees = new ArrayList(); //declare an array of Strings Create a list called MyHotelEmployees and allocate memory for 7 strings
        ArrayList New = new ArrayList<String>(7); //create a new ArrayList

        //Create an instance of an arraylist and asing it to a variable of type list ????


        //Add some elements to that list , like employees using the add() method
        MyHotelEmployees.add("Will Smith");
        MyHotelEmployees.add("Benjamin McCartney");
        MyHotelEmployees.add("Albert Davidson");
        MyHotelEmployees.add("Jaden Clinton");
        MyHotelEmployees.add("Donal Lincoln");
        MyHotelEmployees.add("Bart Bureau");
        MyHotelEmployees.add("Alistaire O'Doherty");

        int totalE = MyHotelEmployees.size();
        //Get the size of the employees by using the size()method and print it
        System.out.println("The number of employees of the hotel is: " + totalE);

        //Print the employees name
        System.out.print("Employee names are: ");
        for (int contor = 0; contor < totalE; contor++) {
            System.out.print(" " + MyHotelEmployees.get(contor).toString() + ',');
        }
        System.out.println();
        //Print the employee at the 1st floor by using his index number
        System.out.println("The employee at first floor is: " + MyHotelEmployees.get(1).toString());

        //remove him by calling the method remove() assigning to his index(eg. myList.remove(0))
        MyHotelEmployees.remove(1);
        System.out.println("The designated employee on the first floor was fired for inappropriate behaviour.");
        totalE = MyHotelEmployees.size();
        System.out.print("The new list of employees is: ");
        for (int contor = 0; contor < totalE; contor++) {
            System.out.print(" " + MyHotelEmployees.get(contor).toString() + ',');
        }
        System.out.println();
        System.out.println("The new number of employees of the hotel is: " + totalE);

        //Scroll through all employees using the for loop
        System.out.println("The new list of employees is: ");
        for (int contor = 0; contor < totalE; contor++) {
            System.out.println(MyHotelEmployees.get(contor).toString());
        }


        // Add a new employee replacing the old employee at the 1st floor using set() method for each
        //employee sort by floor
        MyHotelEmployees.add(1, "Steve Irwin");
        System.out.println();
        System.out.println("The new substitute will be: " + MyHotelEmployees.get(1).toString());
        System.out.println();

        //final step is to print the sorted employees
        totalE = MyHotelEmployees.size();
        System.out.println("The final list of employees is : [");
        for (int contor = 0; contor < totalE; contor++) {
            System.out.print(" " + MyHotelEmployees.get(contor).toString() + ',');
        }
        System.out.println();
    }
}